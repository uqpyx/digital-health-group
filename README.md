# Digital Health Group 8

This is the supplimentary repository for the digital health paper: Developing a Taxonomy of Medical Image Annotation Tools, containing the resource of the research.

Tools:

- [3D-Slicer](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/3d-slicer)
- [CVAT](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/CVAT)
- [Encord](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/Encord)
- [ITK-Snap](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/ITK-Snaps)
- [Kili-Technology](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/kili-technology)
- [LabelBox](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/Labelboxs)
- [MITK-Workbench](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/MITK-Workbenchs)
- [OHIF](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/OHIF)
- [OsiriX](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/OsiriXs)
- [RectLabel](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/RectLabel)
- [Roboflow](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/Roboflow)
- [Supervisely](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/Supervisely)
- [V7Labs](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/v7labs)
- [VGG-Image-Annotator](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/VGG-Image-Annotator)
- [vindr-lab](https://gitlab.kit.edu/uqpyx/digital-health-group/-/tree/main/vindr-lab)
